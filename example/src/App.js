import { createBrowserHistory, createHashHistory } from 'history'
import React from 'react'
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom'
import AuthRoute from 'authfather'

const SinglePage = ({ role, location }) => {
  return (
    <>
      <h1>{location.pathname}</h1>
      <h4>Access: {role}</h4>
      <Link to='/'>Home (no permission)</Link>
      <br />
      <Link to='/p1'>P1 (Permission A1)</Link>
      <br />
      <Link to='/p2'>P2 (Permission A2)</Link>
    </>
  )
}

const history = createBrowserHistory()
const App = (props) => {
  console.log(AuthRoute)
  return (
    <BrowserRouter>
      {/* <Switch> */}
        <Route exact component={SinglePage} path='/' />
        {/* <AuthRoute  component={<SinglePage/>} path="/aa" title={['aa']}  /> */}
        {/* <AuthRoute  /> */}
      {/* </Switch> */}
    </BrowserRouter>
  )
}

export default App
