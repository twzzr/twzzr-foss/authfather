import React from 'react';

const AuthPage = ({ title, children, ...rest }) => {
  useEffect(() => {
    document.title = title.join(' ')
  }, [title])
  return children
}

export default AuthPage;