import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { Redirect, Route } from 'react-router-dom'

const AuthRoute = ({
  component,
  exact,
  redirectLink,
  redirectMessage,
  title,
  path,
  roles,
  authCheck,
  blanket,
  windowRedirect,
  ...rest
}) => {
  console.log('aaa')
  return (
    <Route
      exact={exact}
      path={path}
      component={() => {
        if (typeof roles === 'string') {
          roles = [roles]
        }
        const role = authCheck(roles)
        if (roles.indexOf(role) > -1) {
          return (
            <AuthPage title={title}>
              {blanket(React.cloneElement(component, { role, ...rest }))}
            </AuthPage>
          )
        } else {
          if (windowRedirect) return (window.location.href = redirectLink)
          else
            return (
              <Redirect
                to={{ pathname: redirectLink, message: redirectMessage }}
              />
            )
        }
      }}
    />
  )
}

AuthRoute.propTypes = {
  component: PropTypes.object.isRequired,
  path: PropTypes.string.isRequired,
  title: PropTypes.array.isRequired,
  roles: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  exact: PropTypes.bool,
  redirectLink: PropTypes.string,
  redirectMessage: PropTypes.string,
  authCheck: PropTypes.func,
  blanket: PropTypes.func,
  windowRedirect: PropTypes.bool
}
AuthRoute.defaultProps = {
  exact: true,
  redirectMessage: 'Sorry! (302)',
  authCheck: () => 'public',
  redirectLink: '/',
  roles: ['public'],
  blanket: (children) => children,
  windowRedirect: false
}

export default AuthRoute
