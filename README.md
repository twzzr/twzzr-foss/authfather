# authfather

The vision is basically to extract out as much interface-related code from TwzzrWeb into AuthFather, without making it a burden for any other user installing our library.

### Plans
- AuthRoute: Wrapper over route.
- setAuthRoles: Set different roles.
- roles.getAll(), roles.isClient() etc kind of hooks for us would be useful.